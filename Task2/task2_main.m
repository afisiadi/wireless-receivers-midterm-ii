% Task 2: simulate with time-synch using the oversampling 

clear; clc;


% SNR range to simulate
SNRdB = 0:2:24;

% Load the bits 
load ber_pn_seq
len = 1000000;

% Filterlength
rx_filterlen  = 6*Ltx;

% Load the three rx signals for the three different oversampling factors (The are already modulated to QPSK symbols, pulse shaped and mis-synchronized)
load rx_2x.mat rx1
load rx_4x.mat rx2
load rx_8x.mat rx3

% Oversampling factors for the receivers
L = [2 4 8];

% Roll-off factor
rolloff_factor = 0.11;

% Preamble      %  YOU WILL NEED IT FOR TASK 2.6
preamble_length = 100;
preamble = 1 - 2*lfsr_framesync(preamble_length);

% Initialize bit error rate
BER = zeros(length(SNRdB),length(L));

% String for plot legend
legendString = cell(length(L),1);

for kk = 1:length(L)
    kk
    if kk==1
        rx = rx1;
    elseif kk==2
        rx = rx2;
    elseif kk==3
        rx = rx3;
    end

    
    for ii = 1:length(SNRdB)
        
        % Calculate noise variance
        sigma_2 = 1/10^(SNRdB(ii)/10);
                      
        % Create noise
        w = sqrt(sigma_2/2)*(randn(size(rx)) + 1j*randn(size(rx)));
        
        % Transmit over AWGN channel
        y = rx + w;
        
        % Matched Filter
        pulse_rx = rrc(L(kk), rolloff_factor, rx_filterlen);
        rx_mf = ;               % TO BE COMPLETED
        
        % For Task 2.1 and Task 2.3 get the p0 value from the genie 
        % For Task 2.6 write your function that finds p0 using the preamble
        p0 = ;                  % TO BE COMPLETED
        
        % Remove preamble and Decimate
        data = ;                % TO BE COMPLETED
             
        % Demap
        b_hat = ;               % TO BE COMPLETED
        
        % Calculate BER
        BER(ii,kk) = ;          % TO BE COMPLETED
        
    end
    legendString{kk} = sprintf('L = %d', L(kk));
    
end

% Plot results
figure                          % TO BE COMPLETED

